const http = require('http');
const httpProxy = require('http-proxy');
const url = require('url');
const querystring = require('querystring');

const targetUrl = 'http://todo-list.localhost:8080'; // URL where your site is hosted

const proxy = httpProxy.createProxyServer();

const server = http.createServer((req, res) => {
  if (req.method === 'POST' && req.url === '/add-task') {
    let body = '';

    req.on('data', chunk => {
      body += chunk;
    });

    req.on('end', () => {
      const postData = querystring.parse(body);
      const task = postData.task; // Access the 'task' field from the request body

      // Process the task (e.g., add it to a database, write it to a file, etc.)
      console.log(task);

      // Perform the redirection to tasklist.html
      res.statusCode = 302;
      res.setHeader('Location', '/tasklist.html');
      res.end();
    });
  } else {
    // Proxy other requests to the target URL
    req.headers.host = url.parse(targetUrl).host;
    proxy.web(req, res, { target: targetUrl });
  }
});

server.listen(3000, () => {
  console.log('Server is running on port 8080');
});
