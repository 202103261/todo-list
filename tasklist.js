window.addEventListener('DOMContentLoaded', () => {
  const taskListContainer = document.getElementById('task-list-container');

  // Fetch task list data from server or use hardcoded data
  const tasks = [
    { id: 1, title: 'Task 1', completed: false },
    { id: 2, title: 'Task 2', completed: true },
    // Add more tasks as needed
  ];

  // Generate HTML for the task list
  const taskListHTML = tasks.map(task => `
    <div>
      <span>${task.title}</span>
      <input type="checkbox" ${task.completed ? 'checked' : ''} disabled>
    </div>
  `).join('');

  // Render the task list in the container
  taskListContainer.innerHTML = taskListHTML;
});
