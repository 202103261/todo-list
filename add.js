$(document).ready(function () {
    // Capturar el evento de envío del formulario
    $("#todoForm").submit(function (event) {
        event.preventDefault(); // Evitar el envío del formulario

        // Obtener el texto de la tarea
        var todoText = $("#todoInput").val();

        // Crear el elemento de la tarea
        var todoItem = $("<div></div>").addClass("todo-item");
        var checkbox = $("<input>").attr("type", "checkbox");
        var span = $("<span></span>").text(todoText);

        // Agregar el elemento de la tarea al listado
        todoItem.append(checkbox, span);
        $("#todoList").append(todoItem);

        // Limpiar el campo de entrada
        $("#todoInput").val("");
    });

    // Capturar el evento de cambio de estado del checkbox
    $(document).on("change", ".todo-item input[type='checkbox']", function () {
        var todoItem = $(this).parent();
        todoItem.toggleClass("completed");
    });

    // Capturar el evento de clic en el botón de eliminar
    $("#deleteBtn").click(function () {

        // Obtener todas las tareas seleccionadas
        var selectedItems = $(".todo-item input[type='checkbox']:checked").parent();

        // Eliminar las tareas seleccionadas
        selectedItems.remove();
    });
});